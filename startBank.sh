bank=$1

cd $bank
source .env
docker-compose up -d

cd ..
./create-discovery-file.sh $bank $NODE_DNS_NAME 
./add-peers-from-discovery.sh $bank http://localhost:8081 http://localhost:8082 http://localhost:8083
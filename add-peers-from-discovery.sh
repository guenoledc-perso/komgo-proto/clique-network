#!/bin/bash
bank=$1
shift
targets=$@

cd $bank
for target in $targets
do
    echo "Processing $target"
    ENODES=$(curl --silent $target/discovery.txt)

    for N in $ENODES 
    do
        # remove possible carriage return and new lines
        N=$(echo $N | tr -d "\r" | tr -d "\n")
        echo "ADDING NODE $N"
        docker-compose exec node geth attach --preload /scripts/add-peers.js --exec "main(\"$N\")" /data/geth.ipc
    done

done
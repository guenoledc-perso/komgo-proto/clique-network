
bank=$1
dnsName=$2
targetPath=html

cd $bank
enode=$(docker-compose exec node geth attach --preload /scripts/extract-enode.js --exec "main('$dnsName')" /data/geth.ipc|grep enode)
echo "Creating discovery file for $bank at $targetPath/discovery.txt"
echo $enode > $targetPath/discovery.txt
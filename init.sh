
docker network inspect komgo-internet > /dev/null
if [ $? == 1 ]
then
    docker network create --attachable --subnet 10.0.0.0/8  komgo-internet
fi

banks="bank1 bank2 bank3"
extraData="0x0000000000000000000000000000000000000000000000000000000000000000"
for bank in $banks
do
    mkdir -p $bank/keystore
    rm -Rf $bank/geth*
    cp ./password $bank/keystore/password
    rm -f $bank/.env
    ln -s ../$bank.env $bank/.env

    hasAccount=$(ls $bank/keystore/UTC* | wc -l)
    if [ $hasAccount == 0 ]
    then
        echo "---------------------------------------------------"
        echo "### GENERATING A COINBASE ACCOUNT FOR BANK $bank"
        echo "### WRITE THE ACCOUNT NUMBER TO THE $bank.env file"
        echo "---------------------------------------------------"
        docker run --rm -it -v $(pwd):/data  ethereum/client-go:stable --nousb  --datadir /data/$bank account new --password /data/$bank/keystore/password        
    fi
    account=$(docker run --rm  -v $(pwd):/data  ethereum/client-go:stable --nousb  --datadir /data/$bank account list | grep "#0" | cut -b 14-53)
    echo "---------------------------------------------------"
    echo "ACCOUNT: 0x$account"
    echo "---------------------------------------------------"
    extraData="${extraData}${account}"
    mkdir -p $bank/html
    cp docker-compose-template.yml $bank/docker-compose.yml
done

extraData="${extraData}0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
echo "----------"
echo "You may need to set the genesis.json extraData to the following string then restart the init process to enable all banks to be signers"
echo ${extraData}
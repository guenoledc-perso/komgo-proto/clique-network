

banks="bank1 bank2 bank3"

for bank in $banks
do
    mkdir -p $bank
    rm -Rf $bank/geth*

    docker run --rm -it -v $(pwd):/data  ethereum/client-go:stable --nousb  --datadir /data/$bank init /data/genesis.json
done


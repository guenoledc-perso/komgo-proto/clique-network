
function controlFormat(enodeString) {
    //console.log("Controlling", enodeString)
    if(enodeString.substr(0,8) !=  "enode://") return null
    var parts = enodeString.replace("enode://","").split("@")
    if(parts.length != 2) return null
    var nodekey = parts[0]
    var target = parts[1].split(":")

    if(target.length != 2) return null
    if(Number(target[1]) == Number.NaN) return null

    return "enode://"+nodekey+"@"+target[0]+":"+target[1]
}

function main(enodeString) {
    //console.log("Starting ", enodeString)
    var enode=controlFormat(enodeString)
    if(!enode) {
        console.error("Incorrect enode format:", enodeString)
        return false
    }
    if(!admin) {
        console.error("The admin api is not available in the console")
        return false
    }
    try {
        return admin.addPeer(enode)
        //return admin.addTrustedPeer(enode)
    } catch (error) {
        console.error("Fail adding peer: ", error)
        return false
    }
}
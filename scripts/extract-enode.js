function extractEnode(fqdn) {
    var enode = admin.nodeInfo.enode;
    var node = {
        key: enode.split("@")[0].replace("enode://",""),
        ip: admin.nodeInfo.ip,
        port: admin.nodeInfo.ports.listener,
        discPort: admin.nodeInfo.ports.discovery,
    }
    node.external = "enode://"+node.key+"@"+fqdn+":"+node.port
    return node
}

function main(fqdn) {
    var node = extractEnode(fqdn)
    
    console.log(node.external)
    return 0
}
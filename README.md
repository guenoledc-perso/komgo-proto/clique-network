# Building a dockerized clicque Geth network with simulating participating banks

## Preparation
* Work on a linux / macOs environment
* with docker and docker-compose installed
* Download this repo in a local folder

The scripts will download `nginx:stable-alpine` and `ethereum/client-go:stable` docker image to your machine

## Initialize the 3 banks
Run 
```sh
./init.sh
```
It will 
* Create 3 folders for the 3 banks (bank1, bank2, bank3), then for each bank it will
* Prepare the bank's folder 
* initialize the chain with a genesis file
* create a new account if not already created
* tells you to set the account and genesis extraData in the configuration : **see below explanatons**

Expected output:
```
---------------------------------------------------
### GENERATING A COINBASE ACCOUNT FOR BANK bank1
### WRITE THE ACCOUNT NUMBER TO THE bank1.env file
---------------------------------------------------
INFO [05-03|16:07:50.668] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:07:50.669] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"

Your new key was generated

Public address of the key:   0xacfFE754e2b30E0409f521AB28F1157523025ddB
Path of the secret key file: /data/bank1/keystore/UTC--2020-05-03T16-07-50.684586400Z--acffe754e2b30e0409f521ab28f1157523025ddb

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!

INFO [05-03|16:08:06.155] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:08:06.156] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
---------------------------------------------------
ACCOUNT: 0xacffe754e2b30e0409f521ab28f1157523025ddb
---------------------------------------------------
ls: bank2/keystore/UTC*: No such file or directory
---------------------------------------------------
### GENERATING A COINBASE ACCOUNT FOR BANK bank2
### WRITE THE ACCOUNT NUMBER TO THE bank2.env file
---------------------------------------------------
INFO [05-03|16:08:08.916] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:08:08.916] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"

Your new key was generated

Public address of the key:   0xd9a9baCEfD3bE27a75421894e2134299d68f594b
Path of the secret key file: /data/bank2/keystore/UTC--2020-05-03T16-08-08.933467800Z--d9a9bacefd3be27a75421894e2134299d68f594b

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!

INFO [05-03|16:08:13.354] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:08:13.354] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
---------------------------------------------------
ACCOUNT: 0xd9a9bacefd3be27a75421894e2134299d68f594b
---------------------------------------------------
ls: bank3/keystore/UTC*: No such file or directory
---------------------------------------------------
### GENERATING A COINBASE ACCOUNT FOR BANK bank3
### WRITE THE ACCOUNT NUMBER TO THE bank3.env file
---------------------------------------------------
INFO [05-03|16:08:16.922] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:08:16.924] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"

Your new key was generated

Public address of the key:   0x96AA7fa0FF34037c129Afb0f72A3591e175Da11C
Path of the secret key file: /data/bank3/keystore/UTC--2020-05-03T16-08-16.939804700Z--96aa7fa0ff34037c129afb0f72a3591e175da11c

- You can share your public address with anyone. Others need it to interact with you.
- You must NEVER share the secret key with anyone! The key controls access to your funds!
- You must BACKUP your key file! Without the key, it's impossible to access account funds!
- You must REMEMBER your password! Without the password, it's impossible to decrypt the key!

INFO [05-03|16:08:21.127] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|16:08:21.128] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
---------------------------------------------------
ACCOUNT: 0x96aa7fa0ff34037c129afb0f72a3591e175da11c
---------------------------------------------------
----------
You may need to set the genesis.json extraData to the following string then restart the init process to enable all banks to be signers
0x0000000000000000000000000000000000000000000000000000000000000000acffe754e2b30e0409f521ab28f1157523025ddbd9a9bacefd3be27a75421894e2134299d68f594b96aa7fa0ff34037c129afb0f72a3591e175da11c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
```

## Update the configuration
From the above output, get the accounts id and place them in the bank's config file.   
For instance the extract below gives you the bank1 account to be place in the bank1.env
```
---------------------------------------------------
ACCOUNT: 0xd9a9bacefd3be27a75421894e2134299d68f594b
---------------------------------------------------
```
to be set the `COINBASE` value in `bank1.env`
```
NODE_DNS_NAME=komgo.bank1.com
DISC_DNS_NAME=www.bank1.com
COINBASE=0xd9a9bacefd3be27a75421894e2134299d68f594b
RPC_HOST_PORT=8541
HTTP_HOST_PORT=8081
```

**DO THIS FOR EACH BANK**

Now, update the `genesis.json` file updating the extraData with the output from the first initialization to set the accounts allowed to be initial block signers in the clique consensus

```
"extraData": "0x0000000000000000000000000000000000000000000000000000000000000000acffe754e2b30e0409f521ab28f1157523025ddbd9a9bacefd3be27a75421894e2134299d68f594b96aa7fa0ff34037c129afb0f72a3591e175da11c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
```


## Initialize the blockchain ledger for each bank
Now that the genesis file has been setup, the initialization of the db can be done.
```sh
./initDb.sh
```
Will output the following
```
INFO [05-03|17:16:56.758] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|17:16:56.760] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [05-03|17:16:56.788] Allocated cache and file handles         database=/data/bank1/geth/chaindata cache=16.00MiB handles=16
INFO [05-03|17:16:56.863] Writing custom genesis block 
INFO [05-03|17:16:56.867] Persisted trie from memory database      nodes=1 size=171.00B time=2.096ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:56.887] Successfully wrote genesis state         database=chaindata                  hash=ac5ce8…05ae58
INFO [05-03|17:16:56.888] Allocated cache and file handles         database=/data/bank1/geth/lightchaindata cache=16.00MiB handles=16
INFO [05-03|17:16:56.970] Writing custom genesis block 
INFO [05-03|17:16:56.975] Persisted trie from memory database      nodes=1 size=171.00B time=2.8347ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:56.999] Successfully wrote genesis state         database=lightchaindata                  hash=ac5ce8…05ae58
INFO [05-03|17:16:58.339] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|17:16:58.341] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [05-03|17:16:58.375] Allocated cache and file handles         database=/data/bank2/geth/chaindata cache=16.00MiB handles=16
INFO [05-03|17:16:58.449] Writing custom genesis block 
INFO [05-03|17:16:58.455] Persisted trie from memory database      nodes=1 size=171.00B time=2.0337ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:58.476] Successfully wrote genesis state         database=chaindata                  hash=ac5ce8…05ae58
INFO [05-03|17:16:58.476] Allocated cache and file handles         database=/data/bank2/geth/lightchaindata cache=16.00MiB handles=16
INFO [05-03|17:16:58.548] Writing custom genesis block 
INFO [05-03|17:16:58.551] Persisted trie from memory database      nodes=1 size=171.00B time=2.2603ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:58.572] Successfully wrote genesis state         database=lightchaindata                  hash=ac5ce8…05ae58
INFO [05-03|17:16:59.585] Maximum peer count                       ETH=50 LES=0 total=50
INFO [05-03|17:16:59.592] Smartcard socket not found, disabling    err="stat /run/pcscd/pcscd.comm: no such file or directory"
INFO [05-03|17:16:59.644] Allocated cache and file handles         database=/data/bank3/geth/chaindata cache=16.00MiB handles=16
INFO [05-03|17:16:59.765] Writing custom genesis block 
INFO [05-03|17:16:59.768] Persisted trie from memory database      nodes=1 size=171.00B time=2.2276ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:59.813] Successfully wrote genesis state         database=chaindata                  hash=ac5ce8…05ae58
INFO [05-03|17:16:59.813] Allocated cache and file handles         database=/data/bank3/geth/lightchaindata cache=16.00MiB handles=16
INFO [05-03|17:16:59.900] Writing custom genesis block 
INFO [05-03|17:16:59.904] Persisted trie from memory database      nodes=1 size=171.00B time=4.1339ms gcnodes=0 gcsize=0.00B gctime=0s livenodes=1 livesize=0.00B
INFO [05-03|17:16:59.920] Successfully wrote genesis state         database=lightchaindata                  hash=ac5ce8…05ae58
```

## Starting the banks setup in docker
Each bank's folder contains a `docker-compose.yml` copied from the template and a `.env` configuration file.   
Run the following from the repo directory. It will run for each bank 2 processes
* the `geth` process responsible for the blockchain protocol and storage
* the `nginx` process to deliver over http the `discovery.txt` file of the `geth` enode.
```sh
./startBank.sh bank1
./startBank.sh bank2
./startBank.sh bank3
```

The script will start the `geth` and `nginx` process and then extract the `enode` and write it to the `discovery.txt` file with the internet accessible name of the geth process.    
Then the process collects the other banks' enodes via their `discovery.txt` files and uses it to populate the `geth` peers so it can enters in communication with them.

